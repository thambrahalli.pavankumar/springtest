FROM maven:3.8.4-jdk-11 AS builder
WORKDIR /app
COPY apiservices/src /apiservices/src
COPY apiservices/pom.xml /apiservices
RUN mvn -f /apiservices/pom.xml clean package

FROM openjdk:11 as runner
COPY   --from=builder /apiservices/target/apiservices-1.0.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
