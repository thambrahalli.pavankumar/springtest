package com.riyadbank.apiservices.api.impl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.riyadbank.apiservices.api.AccountService;

@RestController
public class AccountServiceImpl implements AccountService {

	
	@GetMapping("/welcome")
	public String welcome() {		
		return "Digital Services - DIC Team 786123";
	}

}
