package com.riyadbank.apiservices.model;

import lombok.Data;

@Data
public class AccountBalanceResponse {
		
	private String accountNumber;
	private String balance;
	

}
